extractandrename
=========

### About ###

This is a script for an Acrobat X Pro Action that extracts all pages from a multi-page pdf and renames them based on page content (using getPageNthWord() method). User is prompted to enter a number that corresponds to the location of the desired filename. Once the location is confirmed, pages are extracted, renamed, and saved in the directory where the original file is stored. Note that this script is only useful if the desired filename is in the same location on each page. 

### Dependencies ###

Acrobat X Pro

### How To Use ###

Create a custom action

1. At the top right in Acrobat, click the Tools pane. Open the Action Wizard panel.
2. Click Create New Action.
3. Under More Tools on the left, select the "Execute Javascript" tool and click the plus sign to move it
into place on the right.
4. Copy and paste this code into the Javascript Editor, click OK.
5. Click Save. Type a name and description for your action. Click Save. The action appears in the
list of actions.