/* This script extracts all pages in a pdf and renames each page based on content */

var wordIndex = -1;
var newFileName = "";
var fileNameFound = false;
var canceled = false;
var reply ="";
var floor = Math.floor //converts strings to ints, removes user entered decimals 

do { 
	reply = app.response({
		cQuestion: "There are " + this.getPageNumWords(0) + " words on the first page of this document.\n Enter a number between 1 and " + this.getPageNumWords(0) + " corresponding to the position of the desired file name",
		cTitle: "Find file name location",
		cDefault: "",
		bPassword: false,
		cLabel: "Position of file name"});
	
	if(reply == null) {
                //cancel button pressed, leave loop
                this.canceled = true;
	
	// if reply is not a number
	} else if(isNaN(reply)) { 
		app.alert({
			cMsg:"Input value must be numeric", 
			nIcon:1
		});
       
	// if reply is a number 
	} else if(floor(reply) < 1 || floor(reply) > this.getPageNumWords(0)) {	
		app.alert({
			cMsg:"Input value must be between 1 and " + this.getPageNumWords(0), 
			nIcon:1
		});
      
	} else {
		this.selectPageNthWord(0, floor(reply-1)); //word array zero-based. Subtract one to get index of intended word
                var confirmation = app.alert({
                    	cMsg:"Is the selected word the location of the desired file name?",
		nIcon:2,
                    	nType:2
                });
                
                if(confirmation == 4) {
	        this.fileNameFound = true;
	        this.wordIndex = floor(reply-1);
	    }

	}
}
while (!this.fileNameFound && !canceled);
	
if(!canceled && this.wordIndex >= 0 && this.wordIndex < this.getPageNumWords(0)) {
 
    try {for (var i = 0; i < this.numPages; i++) {
	newFileName = this.getPageNthWord(i,this.wordIndex) + ".pdf";
            this.extractPages({
		nStart: i,
		cPath: this.path.replace(this.documentFileName,newFileName)
	});
    }
    } catch (e) { console.println("Aborted: " + e) }
}



